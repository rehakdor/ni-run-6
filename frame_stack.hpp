#ifndef FRAME_STACK_H
#define FRAME_STACK_H

#include "types.hpp"
#include "heap.hpp"

#include <vector>

class Frame{
public:
    const fml_ip return_address;
    std::vector<RTO *> locals;

    Frame(fml_ip preturn_address, size_t locals_count, RTO& filler)
    : return_address(preturn_address), locals(locals_count, &filler){
    }

    void mark(void);
};

class FrameStack{
protected:
    std::vector<Frame *> frames;

    Frame& getFrame(void);
    const Frame& getFrame(void) const;

public:
    ~FrameStack(void);
    void cleanup(void);

    void addFrame(Frame& frame);
    void removeFrame(void);

    fml_ip getReturnAddress(void) const;
    RTO& getLocal(fml_local_index index) const;
    void setLocal(fml_local_index local_index, RTO& rto);

    void mark(void);
};

#endif
