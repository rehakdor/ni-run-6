#ifndef UTIL_HPP
#define UTIL_HPP

#include <iostream>
#include <cstdint>

#define MY_ASSERT(cond) \
do{ \
    if(!(cond)){ \
        std::cerr << "Assertion failed: " << #cond << " (" << __FILE__ << ":" << __LINE__ << ")" << std::endl; \
        std::abort(); \
    } \
}while(0)

// endianess conversion
// WARNING: GCC/Clang specific
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    #define LE2NATIVE16(input) __builtin_bswap16((input));
    #define LE2NATIVE32(input) __builtin_bswap32((input));
#else
    #define LE2NATIVE16(input) (input)
    #define LE2NATIVE32(input) (input)
#endif

#define MAX2(a, b) ((a) > (b) ? (a) : (b))

#endif
