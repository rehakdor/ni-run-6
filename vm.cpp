#include "vm.hpp"

#include "exception.hpp"
#include "util.hpp"
#include "constant_pool.hpp"
#include "heap.hpp"
#include "operand_stack.hpp"
#include "code_vector.hpp"
#include "frame_stack.hpp"
#include "globals.hpp"
#include "labels.hpp"
#include "types.hpp"

#include <fstream>
#include <cstdint>
#include <memory>
#include <iostream>
#include <cstring>
#include <algorithm>

enum Tags{
    Integer = 0x00,
    Boolean = 0x06,
    Null = 0x01,
    String = 0x02,
    Slot = 0x04,
    Method = 0x03,
    Class = 0x05
};

enum Opcodes{
    Literal = 0x01,
    Print = 0x02,
    Drop = 0x10,
    GetGlobal = 0x0C,
    SetGlobal = 0x0B,
    Label = 0x00,
    Jump = 0x0E,
    Branch = 0x0D,
    CallFunction = 0x08,
    Return = 0x0F,
    GetLocal = 0x0A,
    SetLocal = 0x09,
    Object = 0x04,
    GetField = 0x05,
    SetField = 0x06,
    CallMethod = 0x07,
    Array = 0x03
};

size_t VM::opcodeArgSize(uint8_t opcode){
    switch(opcode){
        case Opcodes::Drop:
        case Opcodes::Return:
        case Opcodes::Array:
            return 0;
        case Opcodes::Literal:
        case Opcodes::GetGlobal:
        case Opcodes::SetGlobal:
        case Opcodes::Label:
        case Opcodes::Branch:
        case Opcodes::GetLocal:
        case Opcodes::SetLocal:
        case Opcodes::Object:
        case Opcodes::GetField:
        case Opcodes::SetField:
        case Opcodes::Jump:
            return 2;
        case Opcodes::Print:
        case Opcodes::CallMethod:
        case Opcodes::CallFunction:
            return 3;
    }
    throw InvalidBytecode("Unknown opcode size: " + std::to_string(opcode));
}
CPOInteger *VM::loadCPOInteger(std::ifstream& file){
    fml_int value;
    file.read((char *)&value, sizeof(value));
    value = LE2NATIVE32(value);
    return new CPOInteger(value);
}
CPOBoolean *VM::loadCPOBoolean(std::ifstream& file){
    uint8_t value;
    file.read((char *)&value, sizeof(value));
    return new CPOBoolean(value);
}
CPOString *VM::loadCPOString(std::ifstream& file){
    uint32_t length;
    file.read((char *)&length, sizeof(length));
    length = LE2NATIVE32(length);
    std::unique_ptr<char[]> data(new char[length]);
    file.read(data.get(), length);
    return new CPOString(std::string(data.get(), length));
}
CPOSlot *VM::loadCPOSlot(std::ifstream& file){
    fml_cp_index index;
    file.read((char *)&index, sizeof(index));
    index = LE2NATIVE16(index);
    return new CPOSlot(index);
}
CPOMethod *VM::loadCPOMethod(std::ifstream& file){
    fml_cp_index name_index;
    uint8_t argument_count;
    fml_local_index local_count;
    uint32_t instruction_count;

    file.read((char *)&name_index, sizeof(name_index));
    name_index = LE2NATIVE16(name_index);
    file.read((char *)&argument_count, sizeof(argument_count));
    file.read((char *)&local_count, sizeof(local_count));
    local_count = LE2NATIVE16(local_count);
    file.read((char *)&instruction_count, sizeof(instruction_count));
    instruction_count = LE2NATIVE32(instruction_count);

    // compute code length
    size_t code_vector_start = code_vector.size();
    std::streampos file_start = file.tellg();
    for(size_t i = 0; i < instruction_count; i++){
        uint8_t opcode;
        file.read((char *)&opcode, sizeof(opcode));
        // register labels
        if(opcode == Opcodes::Label){
            fml_cp_index name_index;
            file.read((char *)&name_index, sizeof(name_index));
            name_index = LE2NATIVE16(name_index);
            labels.addLabel(name_index, code_vector_start + file.tellg() - file_start);
        }else
            file.seekg(opcodeArgSize(opcode), std::ios_base::cur);
    }

    // insert into code vector
    size_t length = file.tellg() - file_start;
    file.seekg(file_start, std::ios_base::beg);
    code_vector.extend(length);
    file.read((char *)code_vector.getWritable(code_vector_start), length);

    return new CPOMethod(name_index, argument_count, local_count, code_vector_start, length);
}
CPOClass *VM::loadCPOClass(std::ifstream& file){
    uint16_t item_count;
    file.read((char *)&item_count, sizeof(item_count));
    item_count = LE2NATIVE16(item_count);
    std::vector<fml_cp_index> fields;
    std::vector<fml_cp_index> methods;
    for(size_t i = 0; i < item_count; i++){
        fml_cp_index item;
        file.read((char *)&item, sizeof(item));
        item = LE2NATIVE16(item);
        const CPO& item_cpo = constant_pool.getObject(item);
        // filter into methods and slots
        if(item_cpo.getType() == CPO::Type::Slot){
            const CPOSlot& slot_cpo = static_cast<const CPOSlot&>(item_cpo);
            fields.push_back(slot_cpo.index);
        }else if(item_cpo.getType() == CPO::Type::Method)
            methods.push_back(item);
        else
            throw InvalidBytecode("Class contains something not an item or method");
    }
    return new CPOClass(fields, methods);
}
#define loadCPO_entry(arg) \
case Tags::arg: \
    return loadCPO##arg(file)
CPO *VM::loadCPO(std::ifstream& file){
    uint8_t tag;
    file.read((char *)&tag, sizeof(tag));
    switch(tag){
        loadCPO_entry(Integer);
        loadCPO_entry(Boolean);
        loadCPO_entry(String);
        loadCPO_entry(Slot);
        loadCPO_entry(Method);
        loadCPO_entry(Class);
        case Tags::Null:
            return new CPONull;
    }
    throw InvalidBytecode("Unknown tag");
}
void VM::loadConstantPool(std::ifstream& file){
    uint16_t cp_size;
    file.read((char *)&cp_size, sizeof(cp_size));
    cp_size = LE2NATIVE16(cp_size);
    for(size_t i = 0; i < cp_size; i++)
        constant_pool.addObject(*loadCPO(file));
}
void VM::loadGlobals(std::ifstream& file){
    uint16_t gl_size;
    file.read((char *)&gl_size, sizeof(gl_size));
    gl_size = LE2NATIVE16(gl_size);
    for(size_t i = 0; i < gl_size; i++){
        fml_cp_index target;
        file.read((char *)&target, sizeof(target));
        target = LE2NATIVE16(target);
        const CPO& cpo_target = constant_pool.getObject(target);
        if(cpo_target.getType() == CPO::Type::Method){
            const CPOMethod& cpo_method = static_cast<const CPOMethod&>(cpo_target);
            globals.addMethod(cpo_method.name_index, target);
        } // TODO: what am i supposed to actually do with Slot?
    }
}
void VM::loadEntry(std::ifstream& file){
    fml_cp_index method;
    file.read((char *)&method, sizeof(method));
    method = LE2NATIVE16(method);
    entry = method;
}



RTO& VM::CPO2RTO(const CPO& cpo){
    switch(cpo.getType()){
        case CPO::Type::Null:
            return rto_allocator.newNull();
        case CPO::Type::Boolean:{
            const CPOBoolean& real_cpo = static_cast<const CPOBoolean&>(cpo);
            return rto_allocator.newBoolean(real_cpo.value);
        }
        case CPO::Type::Integer:{
            const CPOInteger& real_cpo = static_cast<const CPOInteger&>(cpo);
            return rto_allocator.newInteger(real_cpo.value);
        }
        default:
            throw InvalidBytecode("Requested to create an impossible run-time object from a constant pool object");
    }
}
void VM::runLiteral(void){
    fml_cp_index index = code_vector.getCPIndex(&ip);
    const CPO& cpo = constant_pool.getObject(index);
    operand_stack.push(CPO2RTO(cpo));
}
void VM::printObject(const RTOObject& rto){
    std::cout << "object(";
    bool first = true;
    if(rto.parent->getType() == RTO::Type::Object){
        std::cout << "..=";
        printObject(static_cast<const RTOObject&>(*rto.parent));
        first = false;
    }
    for(auto pair : rto.fields){
        if(first)
            first = false;
        else
            std::cout << ", ";
        const CPOString& name = static_cast<const CPOString&>(constant_pool.getObject(pair.first));
        std::cout << name.value << "=";
        printRTO(*pair.second);
    }
    std::cout << ")";
}
void VM::printArray(const RTOArray& rto){
    std::cout << "[";
    bool first = true;
    for(auto heap_ptr : rto.data){
        if(first)
            first = false;
        else
            std::cout << ", ";
        printRTO(*heap_ptr);
    }
    std::cout << "]";
}
void VM::printRTO(const RTO& rto){
    switch(rto.getType()){
        case RTO::Type::Null:
            std::cout << "null";
            break;
        case RTO::Type::Boolean:
            std::cout << (static_cast<const RTOBoolean&>(rto).value ? "true" : "false");
            break;
        case RTO::Type::Integer:
            std::cout << static_cast<const RTOInteger&>(rto).value;
            break;
        case RTO::Type::Object:
            printObject(static_cast<const RTOObject&>(rto));
            break;
        case RTO::Type::Array:
            printArray(static_cast<const RTOArray&>(rto));
            break;
    }
}
#define BACKLASH_ITEM(input, output) \
    case input: \
    std::cout << output; \
    break
void VM::runPrint(void){
    fml_cp_index format_index = code_vector.getCPIndex(&ip);
    uint8_t argument_count = code_vector.get8(&ip);
    const CPO& format = constant_pool.getObject(format_index);
    if(format.getType() != CPO::Type::String)
        throw InvalidBytecode("Print format object is not of type string");
    const CPOString& real_format = static_cast<const CPOString&>(format);
    std::vector<RTO *> arguments;
    for(size_t i = 0; i < argument_count; i++)
        arguments.push_back(&operand_stack.pop());
    std::reverse(arguments.begin(), arguments.end());

    // This whole block is largely taken from homework 2A
    const char *c_str = real_format.value.c_str();
    size_t i = 0;
    size_t curr_arg = 0;
    bool backlash_mode = false;
    for(char curr_char; (curr_char = c_str[i]) != '\0'; i++){
        if(backlash_mode){
            switch(curr_char){
                BACKLASH_ITEM('~', "~");
                BACKLASH_ITEM('n', std::endl);
                BACKLASH_ITEM('"', "\"");
                BACKLASH_ITEM('t', "\t");
                BACKLASH_ITEM('\\', "\\");
                case 'r':
                    break; // doesnt work in C++
                default:
                    throw InvalidBytecode("Unknown \\ operator");
            }
            backlash_mode = false;
        }else{
            if(curr_char == '\\')
                backlash_mode = true;
            else if(curr_char == '~'){
                printRTO(*arguments[curr_arg]);
                curr_arg++;
            }else
                std::cout << curr_char;
        }
    }
    if(backlash_mode)
        throw InvalidBytecode("Print cannot end with \\");

    // put the null RTO on stack
    operand_stack.push(rto_allocator.newNull());
}
void VM::runDrop(void){
    operand_stack.pop();
}
void VM::runGetGlobal(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    RTO& rto = globals.getSlot(name_index);
    operand_stack.push(rto);
}
void VM::runSetGlobal(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    RTO& rto = operand_stack.peek(0);
    globals.addSlot(name_index, rto);
}
void VM::runLabel(void){
    ip += 2; // labels are already processed when loading method CPOs
}
void VM::runJump(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    ip = labels.getLabel(name_index);
}
bool VM::evaluateTrue(const RTO& rto){
    switch(rto.getType()){
        case RTO::Type::Null:
            return false;
        case RTO::Type::Boolean:
            return static_cast<const RTOBoolean&>(rto).value;
        default:
            return true;
    }
}
void VM::runBranch(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    RTO& condition_index = operand_stack.pop();
    if(evaluateTrue(condition_index))
        ip = labels.getLabel(name_index);
}
void VM::runCallFunction(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    uint8_t argument_count = code_vector.get8(&ip);
    const CPO& cpo = constant_pool.getObject(globals.getMethod(name_index));
    if(cpo.getType() != CPO::Type::Method)
        throw InvalidBytecode("Calling something not a method");
    const CPOMethod& method = static_cast<const CPOMethod&>(cpo);
    if(argument_count != method.argument_count)
        throw InvalidBytecode("Function call's argument count not matching method definition");

    frame_stack.addFrame(*new Frame(ip, argument_count + method.local_count, rto_allocator.newNull()));
    for(size_t i = argument_count; i > 0; i--){
        RTO& rto = operand_stack.pop();
        frame_stack.setLocal(i - 1, rto);
    }
    ip = method.start;
}
void VM::runReturn(void){
    ip = frame_stack.getReturnAddress();
    frame_stack.removeFrame();
}
void VM::runGetLocal(void){
    fml_local_index local_index = code_vector.getLocalIndex(&ip);
    RTO& rto = frame_stack.getLocal(local_index);
    operand_stack.push(rto);
}
void VM::runSetLocal(void){
    fml_local_index local_index = code_vector.getLocalIndex(&ip);
    RTO& rto = operand_stack.peek(0);
    frame_stack.setLocal(local_index, rto);
}
void VM::runObject(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    const CPO& cpo = constant_pool.getObject(name_index);
    if(cpo.getType() != CPO::Type::Class)
        throw InvalidBytecode("Attempted to instantiate something not a class");
    const CPOClass& class_cpo = static_cast<const CPOClass&>(cpo);
    RTOObject& object_rto = rto_allocator.newObject();
    tmp_root.insert(object_rto);

    for(auto it = class_cpo.fields.rbegin(); it != class_cpo.fields.rend(); ++it){
        RTO& field = operand_stack.pop();
        tmp_root.insert(field);
        object_rto.setField(*it, field);
    }
    RTO& parent_ptr = operand_stack.pop();
    object_rto.setParent(parent_ptr);
    for(auto i : class_cpo.methods){
        const CPOMethod& method_cpo = static_cast<const CPOMethod&>(constant_pool.getObject(i));
        object_rto.setMethod(method_cpo.name_index, i);
    }

    operand_stack.push(object_rto);
}
void VM::runGetField(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    RTO& object_rto = operand_stack.pop();
    if(object_rto.getType() != RTO::Type::Object)
        throw InvalidBytecode("Attempted to load a field on something not an object");
    RTOObject& real_object = static_cast<RTOObject&>(object_rto);
    RTO& result_ptr = real_object.getField(name_index);
    operand_stack.push(result_ptr);
}
void VM::runSetField(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    RTO& value_ptr = operand_stack.pop();
    tmp_root.insert(value_ptr);
    RTO& object_rto = operand_stack.pop();
    tmp_root.insert(object_rto);
    if(object_rto.getType() != RTO::Type::Object)
        throw InvalidBytecode("Attempted to set a field on something not an object");
    RTOObject& real_object = static_cast<RTOObject&>(object_rto);
    real_object.setField(name_index, value_ptr);
    operand_stack.push(value_ptr);
}
RTOBoolean& VM::resolveMethodCallNull(const std::string& name, const std::vector<RTO *>& arguments){
    if(arguments.size() != 1)
        throw InvalidBytecode("Attempted to call a null builtin method with wrong arity");
    bool value = (arguments[0]->getType() == RTO::Type::Null);
    if(name == "==" || name == "eq")
        return rto_allocator.newBoolean(value);
    else if(name == "!=" || name == "neq")
        return rto_allocator.newBoolean(!value);
    throw InvalidBytecode("Attempted to call a non-existent builtin method on null: " + name);
}
bool VM::resolveIntegerEquality(RTOInteger& a, RTO& b){
    if(b.getType() != RTO::Type::Integer)
        return false;
    RTOInteger& real_b = static_cast<RTOInteger&>(b);
    return a.value == real_b.value;
}
RTO& VM::resolveMethodCallInteger(RTOInteger& receiver, const std::string& name, const std::vector<RTO *>& arguments){
    if(arguments.size() != 1)
        throw InvalidBytecode("Attempted to call an integer builtin method with wrong arity");
    if(name == "==" || name == "eq")
        return rto_allocator.newBoolean(resolveIntegerEquality(receiver, *arguments[0]));
    else if(name == "!=" || name == "neq")
        return rto_allocator.newBoolean(!resolveIntegerEquality(receiver, *arguments[0]));
    else{
        if(arguments[0]->getType() != RTO::Type::Integer)
            throw InvalidBytecode("Attempted to call an integer builtin method with wrong argument type");
        RTOInteger& real_arg = static_cast<RTOInteger&>(*arguments[0]);
        if(name == "+" || name == "add")
            return rto_allocator.newInteger(receiver.value + real_arg.value);
        else if(name == "-" || name == "sub")
            return rto_allocator.newInteger(receiver.value - real_arg.value);
        else if(name == "*" || name == "mul")
            return rto_allocator.newInteger(receiver.value * real_arg.value);
        else if(name == "/" || name == "div")
            return rto_allocator.newInteger(receiver.value / real_arg.value);
        else if(name == "%" || name == "mod")
            return rto_allocator.newInteger(receiver.value % real_arg.value);
        else if(name == "<=" || name == "le")
            return rto_allocator.newBoolean(receiver.value <= real_arg.value);
        else if(name == ">=" || name == "ge")
            return rto_allocator.newBoolean(receiver.value >= real_arg.value);
        else if(name == "<" || name == "lt")
            return rto_allocator.newBoolean(receiver.value < real_arg.value);
        else if(name == ">" || name == "gt")
            return rto_allocator.newBoolean(receiver.value > real_arg.value);
    }
    throw InvalidBytecode("Attempted to call a non-existent builtin method on integer: " + name);
}
bool VM::resolveBooleanEquality(RTOBoolean& a, RTO& b){
    if(b.getType() != RTO::Type::Boolean)
        return false;
    RTOBoolean& real_b = static_cast<RTOBoolean&>(b);
    return a.value == real_b.value;
}
RTOBoolean& VM::resolveMethodCallBoolean(RTOBoolean& receiver, const std::string& name, const std::vector<RTO *>& arguments){
    if(arguments.size() != 1)
        throw InvalidBytecode("Attempted to call a boolean builtin method with wrong arity");
    if(name == "==" || name == "eq")
        return rto_allocator.newBoolean(resolveBooleanEquality(receiver, *arguments[0]));
    else if(name == "!=" || name == "neq")
        return rto_allocator.newBoolean(!resolveBooleanEquality(receiver, *arguments[0]));
    else{
        if(arguments[0]->getType() != RTO::Type::Boolean)
            throw InvalidBytecode("Attempted to call a boolean builtin method with wrong argument type");
        RTOBoolean& real_arg = static_cast<RTOBoolean&>(*arguments[0]);
        if(name == "&" || name == "and")
            return rto_allocator.newBoolean(receiver.value && real_arg.value);
        else if(name == "`" || name == "or" || name == "|") // last one is undocumented
            return rto_allocator.newBoolean(receiver.value || real_arg.value);
    }
    throw InvalidBytecode("Attempted to call a non-existent builtin method on boolean: " + name);
}
RTO& VM::resolveMethodCallArray(RTOArray& receiver, const std::string& name, const std::vector<RTO *>& arguments){
    if(arguments.size() < 1)
        throw InvalidBytecode("Array access method missing index");
    RTO& index_rto = *arguments[0];
    if(index_rto.getType() != RTO::Type::Integer)
        throw InvalidBytecode("Array index must be an integer");
    RTOInteger& index = static_cast<RTOInteger&>(index_rto);
    if(index.value < 0)
        throw InvalidBytecode("Array index must be at least 0");
    if(name == "set"){
        if(arguments.size() != 2)
            throw InvalidBytecode("Array setting method needs 2 arguments");
        RTO& value_ptr = *arguments[1];
        receiver.setField(index.value, value_ptr);
        return value_ptr;
    }else if(name == "get"){
        return receiver.getField(index.value);
    }else
        throw InvalidBytecode("Attempted to call a non-existent builtin method on array: " + name);
}
RTO& VM::resolveMethodCallBuiltin(RTO& receiver, const std::string& name, const std::vector<RTO *>& arguments){
    switch(receiver.getType()){
        case RTO::Type::Null:
            return resolveMethodCallNull(name, arguments);
        case RTO::Type::Integer:
            return resolveMethodCallInteger(static_cast<RTOInteger&>(receiver), name, arguments);
        case RTO::Type::Boolean:
            return resolveMethodCallBoolean(static_cast<RTOBoolean&>(receiver), name, arguments);
        case RTO::Type::Array:
            return resolveMethodCallArray(static_cast<RTOArray&>(receiver), name, arguments);
        default:
            throw InvalidBytecode("Attempted to call a non-existent builtin method: " + name);
    }
}
void VM::resolveMethodCall(RTO& receiver_ptr, fml_cp_index name_ptr, const std::vector<RTO *>& arguments_ptr){
    if(receiver_ptr.getType() != RTO::Type::Object){ // search builtin methods
        const CPO& cpo = constant_pool.getObject(name_ptr);
        if(cpo.getType() != CPO::Type::String)
            throw InvalidBytecode("Calling method whose name is not a string");
        const CPOString& name = static_cast<const CPOString&>(cpo);
        RTO& result = resolveMethodCallBuiltin(receiver_ptr, name.value, arguments_ptr);
        operand_stack.push(result);
        return;
    }
    // search for object methods
    RTOObject& receiver_obj = static_cast<RTOObject&>(receiver_ptr);
    auto it = receiver_obj.methods.find(name_ptr);
    if(it == receiver_obj.methods.end()){ // search in parent
        resolveMethodCall(*receiver_obj.parent, name_ptr, arguments_ptr);
        return;
    }
    // found method, execute
    fml_cp_index method_cpi = it->second;
    const CPO& cpo = constant_pool.getObject(method_cpi);
    if(cpo.getType() != CPO::Type::Method)
        throw InvalidBytecode("Attempted to call something not a method");
    const CPOMethod& method = static_cast<const CPOMethod&>(cpo);
    size_t real_argument_count = method.argument_count - 1; // dont count receiver; i wasted 4 hours on this bug alone
    if(arguments_ptr.size() != real_argument_count)
        throw InvalidBytecode("Method call's argument count not matching method definition");

    tmp_root.insert(receiver_ptr);
    for(size_t i = 0; i < real_argument_count; i++)
        tmp_root.insert(*arguments_ptr[i]);
    frame_stack.addFrame(*new Frame(ip, real_argument_count + method.local_count + 1, rto_allocator.newNull()));
    frame_stack.setLocal(0, receiver_ptr);
    for(size_t i = 0; i < real_argument_count; i++)
        frame_stack.setLocal(i + 1, *arguments_ptr[i]);
    ip = method.start;
}
void VM::runCallMethod(void){
    fml_cp_index name_ptr = code_vector.getCPIndex(&ip);
    uint8_t argument_count = code_vector.get8(&ip);
    std::vector<RTO *> arguments_ptr;
    for(size_t i = 0; i < argument_count - 1; i++){
        RTO& argument = operand_stack.pop();
        arguments_ptr.push_back(&argument);
    }
    std::reverse(arguments_ptr.begin(), arguments_ptr.end());
    RTO& receiver_ptr = operand_stack.pop();
    resolveMethodCall(receiver_ptr, name_ptr, arguments_ptr);
}
void VM::runArray(void){
    RTO& default_value = operand_stack.pop();
    tmp_root.insert(default_value);
    RTO& size_rto = operand_stack.pop();
    if(size_rto.getType() != RTO::Type::Integer)
        throw InvalidBytecode("Array size must be an integer");
    RTOInteger& size = static_cast<RTOInteger&>(size_rto);
    RTOArray& result = rto_allocator.newArray(size.value, default_value);
    operand_stack.push(result);
}
#define runIP_entry(arg) \
case Opcodes::arg: \
    run##arg(); \
    break;
void VM::runIP(void){
    while(ip < code_vector.size()){ // TODO: what is the real expected end of execution?
        uint8_t opcode = *code_vector.get(ip);
        ip++;
        switch(opcode){
            runIP_entry(Literal);
            runIP_entry(Print);
            runIP_entry(Drop);
            runIP_entry(GetGlobal);
            runIP_entry(SetGlobal);
            runIP_entry(Label);
            runIP_entry(Jump);
            runIP_entry(Branch);
            runIP_entry(CallFunction);
            runIP_entry(Return);
            runIP_entry(GetLocal);
            runIP_entry(SetLocal);
            runIP_entry(Object);
            runIP_entry(GetField);
            runIP_entry(SetField);
            runIP_entry(CallMethod);
            runIP_entry(Array);
            default:
                throw InvalidBytecode("Unknown opcode: " + std::to_string(opcode));
        }
        tmp_root.cleanup();
        //heap.garbageCollect();
    }
}


VM::VM(size_t heap_size, std::ofstream& plog_file)
: heap(heap_size, plog_file, globals, operand_stack, frame_stack, tmp_root), rto_allocator(heap){
    HeapStdAllocatorState::heap = &heap;
}
VM::~VM(void){
    cleanup();
}
void VM::cleanup(void){
    operand_stack.cleanup();
    globals.cleanup();
    frame_stack.cleanup();
    heap.cleanup();
    tmp_root.cleanup();
}



void VM::loadFile(std::ifstream& file){
    loadConstantPool(file);
    loadGlobals(file);
    loadEntry(file);
}
void VM::run(void){
    const CPOMethod& main = static_cast<const CPOMethod&>(constant_pool.getObject(entry));
    RTONull& null = rto_allocator.newNull();
    frame_stack.addFrame(*new Frame(0, main.local_count, null));
    ip = main.start;
    runIP();
    frame_stack.removeFrame();
    cleanup();
}
