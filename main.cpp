#include "vm.hpp"
#include "exception.hpp"

#include <cstring>
#include <iostream>
#include <fstream>
#include <exception>
#include <sstream>

enum class ArgMode{
    Main,
    HeapSize,
    HeapLog
};

void printArgMessage(void){
    std::cerr << "Missing arguments; <binary> run [--heap-size <heap size in MiB>] [--heap-log <log path>] <bytecode path>" << std::endl;
}

int main(int argc, char *argv[]){
    if(argc < 2 || strcmp("run", argv[1]) != 0){
        printArgMessage();
        goto fail;
    }

    try{
        unsigned int heap_size = 1024;
        std::string input_path;
        std::string log_path;

        ArgMode mode = ArgMode::Main;
        for(int i = 2; i < argc; i++){
            if(mode == ArgMode::Main){
                if(strcmp("--heap-size", argv[i]) == 0)
                    mode = ArgMode::HeapSize;
                else if(strcmp("--heap-log", argv[i]) == 0)
                    mode = ArgMode::HeapLog;
                else if(i + 1 == argc)
                    input_path = std::string(argv[i]);
                else{
                    printArgMessage();
                    goto fail;
                }
            }else if(mode == ArgMode::HeapSize){
                std::istringstream ss(argv[i]);
                if(!(ss >> heap_size) || !ss.eof()){
                    std::cerr << "Invalid heap size: " << argv[i] << std::endl;
                    goto fail;
                }
                mode = ArgMode::Main;
            }else if(mode == ArgMode::HeapLog){
                log_path = std::string(argv[i]);
                mode = ArgMode::Main;
            }
        }

        if(input_path.empty()){
            printArgMessage();
            goto fail;
        }

        std::ifstream input_file(input_path);
        input_file.exceptions(std::istream::eofbit | std::istream::failbit | std::istream::badbit);

        std::ofstream log_file;
        if(!log_path.empty()){
            log_file.exceptions(std::istream::eofbit | std::istream::failbit | std::istream::badbit);
            log_file.open(log_path);
            log_file << "timestamp,event,heap" << std::endl;
        }

        VM vm(heap_size * 1024 * 1024, log_file);
        vm.loadFile(input_file);
        input_file.close();

        vm.run();
    }catch(InvalidBytecode& e){
        std::cerr << "Invalid bytecode: " << e.what() << std::endl;
    }catch(std::exception& e){
        std::cerr << e.what() << std::endl;
        goto fail;
    }catch(...){
        std::cerr << "Unknown object thrown" << std::endl;
        goto fail;
    }

    return 0;

    fail:
    std::cout << "Failure" << std::endl;
    return 1;
}
