#include "operand_stack.hpp"

#include "heap.hpp"
#include "types.hpp"

#include <vector>

OperandStack::~OperandStack(void){
    cleanup();
}
void OperandStack::cleanup(void){
    stack.clear();
}

RTO& OperandStack::pop(void){
    RTO *result = stack.back();
    stack.pop_back();
    return *result;
}
void OperandStack::push(RTO& pointer){
    stack.push_back(&pointer);
}
RTO& OperandStack::peek(size_t index){
    RTO *result = stack[stack.size() - index - 1];
    return *result;
}
size_t OperandStack::size(void){
    return stack.size();
}

void OperandStack::mark(void){
    for(auto it : stack)
        it->mark();
}
