#include "globals.hpp"

#include "util.hpp"
#include "types.hpp"

#include <map>

Globals::~Globals(void){
    cleanup();
}
void Globals::cleanup(void){
    slots.clear();
}

void Globals::addSlot(fml_cp_index name, RTO& rto){
    auto it = slots.find(name);
    if(it != slots.end()){
        it->second = &rto;
    }else
        slots.emplace(name, &rto);
}
void Globals::addMethod(fml_cp_index name, fml_cp_index method){
    MY_ASSERT(methods.emplace(name, method).second);
}
RTO& Globals::getSlot(fml_cp_index name){
    auto it = slots.find(name);
    MY_ASSERT(it != slots.end());
    return *it->second;
}
fml_cp_index Globals::getMethod(fml_cp_index name){
    auto it = methods.find(name);
    MY_ASSERT(it != methods.end());
    return it->second;
}

void Globals::mark(void){
    for(auto it : slots)
        it.second->mark();
}
