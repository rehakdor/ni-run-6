#include "constant_pool.hpp"

#include <vector>


CPO::Type CPONull::getType(void) const{
    return Type::Null;
}
CPO::Type CPOBoolean::getType(void) const{
    return Type::Boolean;
}
CPO::Type CPOInteger::getType(void) const{
    return Type::Integer;
}
CPO::Type CPOString::getType(void) const{
    return Type::String;
}
CPO::Type CPOSlot::getType(void) const{
    return Type::Slot;
}
CPO::Type CPOMethod::getType(void) const{
    return Type::Method;
}
CPO::Type CPOClass::getType(void) const{
    return Type::Class;
}

ConstantPool::~ConstantPool(void){
    for(auto cpo : objects)
        delete cpo;
}

void ConstantPool::addObject(CPO& object){
    objects.push_back(&object);
}
const CPO& ConstantPool::getObject(fml_cp_index index) const{
    return *objects[index];
}
