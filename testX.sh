#!/bin/bash

for t in tests*; do
    for i in "$t"/*.bc; do
        echo =======================================
        echo "Running $i"
        #valgrind --leak-check=full ./main run --heap-size 16 --heap-log log.txt "$i"
        ./main run --heap-size 1 "$i"
        echo
    done
done
