#!/bin/bash

set -e

REAL_FML=/home/dorian/CVUT/RUN/FML/FML/target/release/fml
REHAKDOR_FML=/home/dorian/CVUT/RUN/6/main

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --heap-size)
    HEAP_SIZE="$2"
    shift
    shift
    ;;
    --heap-log)
    HEAP_LOG="$2"
    shift
    shift
    ;;
    run)
    shift
    ;;
    *)
    POSITIONAL+=("$1")
    shift
    ;;
esac
done
set -- "${POSITIONAL[@]}"

FML_PATH="$1"

"$REAL_FML" parse --format lisp -o tmp.lisp "$FML_PATH"
"$REAL_FML" compile --input-format lisp --output-format bytes -o tmp.bc tmp.lisp
if [ -n "$HEAP_SIZE" ]; then
    ARG="--heap-size $HEAP_SIZE $ARG"
fi
if [ -n "$HEAP_LOG" ]; then
    ARG="--heap-log \"$HEAP_LOG\" $ARG"
fi
"$REHAKDOR_FML" run $ARG tmp.bc
rm tmp.bc tmp.lisp
