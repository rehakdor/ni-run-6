#ifndef VM_H
#define VM_H

#include "constant_pool.hpp"
#include "heap.hpp"
#include "operand_stack.hpp"
#include "code_vector.hpp"
#include "frame_stack.hpp"
#include "globals.hpp"
#include "labels.hpp"
#include "tmp_root.hpp"
#include "types.hpp"

#include <fstream>
#include <cstdint>

class VM{
protected:
    // returns remaining size of an instruction after an opcode was read
    size_t opcodeArgSize(uint8_t opcode);
    // these methods create constant pool objects
    CPOInteger *loadCPOInteger(std::ifstream& file);
    CPOBoolean *loadCPOBoolean(std::ifstream& file);
    CPOString *loadCPOString(std::ifstream& file);
    CPOSlot *loadCPOSlot(std::ifstream& file);
    CPOMethod *loadCPOMethod(std::ifstream& file);
    CPOClass *loadCPOClass(std::ifstream& file);
    CPO *loadCPO(std::ifstream& file);
    void loadConstantPool(std::ifstream& file);
    void loadGlobals(std::ifstream& file);
    void loadEntry(std::ifstream& file);

    void registerLabels(void);

    // creates a runtime object from a constant pool object, inserts it in heap, returns pointer to heap
    RTO& CPO2RTO(const CPO& cpo);

    // individual instructions and their helper functions
    void runLiteral(void);
    void printObject(const RTOObject& rto);
    void printArray(const RTOArray& rto);
    void printRTO(const RTO& rto);
    void runPrint(void);
    void runDrop(void);
    void runGetGlobal(void);
    void runSetGlobal(void);
    void runLabel(void);
    void runJump(void);
    bool evaluateTrue(const RTO& rto);
    void runBranch(void);
    void runCallFunction(void);
    void runReturn(void);
    void runGetLocal(void);
    void runSetLocal(void);
    void runObject(void);
    void runGetField(void);
    void runSetField(void);
    // null builtin methods
    RTOBoolean& resolveMethodCallNull(const std::string& name, const std::vector<RTO *>& arguments);
    // integer builtin methods
    bool resolveIntegerEquality(RTOInteger& a, RTO& b);
    RTO& resolveMethodCallInteger(RTOInteger& receiver, const std::string& name, const std::vector<RTO *>& arguments);
    // boolean builtin methods
    bool resolveBooleanEquality(RTOBoolean& a, RTO& b);
    RTOBoolean& resolveMethodCallBoolean(RTOBoolean& receiver, const std::string& name, const std::vector<RTO *>& arguments);
    // array builtin methods
    RTO& resolveMethodCallArray(RTOArray& receiver, const std::string& name, const std::vector<RTO *>& arguments);
    // resolves all builtin methods
    RTO& resolveMethodCallBuiltin(RTO& receiver, const std::string& name, const std::vector<RTO *>& arguments);
    void resolveMethodCall(RTO& receiver, fml_cp_index name_ptr, const std::vector<RTO *>& arguments_ptr);
    void runCallMethod(void);
    void runArray(void);
    void runIP(void);

public:
    ConstantPool constant_pool;
    Heap heap;
    HeapRTOAllocator rto_allocator;
    CodeVector code_vector;
    OperandStack operand_stack;
    Globals globals;
    Labels labels;
    FrameStack frame_stack;
    TmpRoot tmp_root;
    fml_cp_index entry;
    fml_ip ip;

    VM(size_t heap_size, std::ofstream& plog_file);
    ~VM(void);
    void cleanup(void);

    void loadFile(std::ifstream& file);
    void run(void);
};

#endif
