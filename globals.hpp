#ifndef GLOBALS_H
#define GLOBALS_H

#include "types.hpp"
#include "heap.hpp"

#include <map>

class Globals{
protected:
    std::map<fml_cp_index, RTO *> slots;
    std::map<fml_cp_index, fml_cp_index> methods;

public:
    ~Globals(void);
    void cleanup(void);

    void addSlot(fml_cp_index name, RTO& rto);
    void addMethod(fml_cp_index name, fml_cp_index method);
    RTO& getSlot(fml_cp_index name);
    fml_cp_index getMethod(fml_cp_index name);

    void mark(void);
};

#endif
