#ifndef EXCEPTION_HPP
#define EXCEPTION_HPP

#include <exception>
#include <stdexcept>

class MyException: public std::runtime_error{
public:
    using runtime_error::runtime_error;
};

class InvalidBytecode: public MyException{
public:
    using MyException::MyException;
};

#endif
