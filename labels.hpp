#ifndef LABELS_H
#define LABELS_H

#include "types.hpp"

#include <map>

class Labels{
protected:
    std::map<fml_cp_index, fml_ip> labels;

public:
    void addLabel(fml_cp_index name, fml_ip address);
    fml_ip getLabel(fml_cp_index name);
};

#endif
