#ifndef HEAP_H
#define HEAP_H

#include "types.hpp"

#include <map>
#include <vector>
#include <list>
#include <memory>
#include <set>

class Heap;
class HeapStdAllocatorState{
public:
    static Heap *heap;
};
template<class T>
class HeapStdAllocator : public HeapStdAllocatorState{
public:
    typedef T value_type;

    T *allocate(size_t n);
    void deallocate(T *p, size_t n) noexcept;
};

// Run-Time Object base
class RTO{
protected:
    bool marked;

public:
    enum class Type{
        Null,
        Boolean,
        Integer,
        Object,
        Array
    };

    RTO(void);
    virtual ~RTO(void) = default;
    virtual Type getType(void) const = 0;

    void clearMark(void);
    void mark(void);
    bool isMarked(void) const;
    virtual void markChildren(void);
};
class RTONull: public RTO{
public:
    Type getType(void) const override;
};
class RTOBoolean: public RTO{
public:
    bool value;

    RTOBoolean(bool pvalue);
    Type getType(void) const override;
};
class RTOInteger: public RTO{
public:
    fml_int value;

    RTOInteger(fml_int pvalue);
    Type getType(void) const override;
};
class RTOObject: public RTO{
public:
    RTO *parent;
    std::map<fml_cp_index, RTO *, std::less<fml_cp_index>, HeapStdAllocator<std::pair<const fml_cp_index, RTO *>>> fields;
    std::map<fml_cp_index, fml_cp_index> methods;

    RTOObject(void);
    Type getType(void) const override;

    void setParent(RTO& pparent);
    void setMethod(fml_cp_index name, fml_cp_index method);
    void setField(fml_cp_index name, RTO& value);
    RTO& getField(fml_cp_index name) const;

    void markChildren(void) override;
};
class RTOArray: public RTO{
public:
    std::vector<RTO *, HeapStdAllocator<RTO *>> data;

    RTOArray(size_t size, RTO& filler);
    Type getType(void) const override;

    void setField(size_t index, RTO& value);
    RTO& getField(size_t index) const;

    void markChildren(void) override;
};

class Globals;
class OperandStack;
class FrameStack;
class TmpRoot;

class Heap{
protected:
    class FreeChunk{
    public:
        void *address;
        size_t size;
        FreeChunk(void *paddress, size_t psize)
        : address(paddress), size(psize){
        }
    };
    std::unique_ptr<char[]> heap_memory;
    std::set<RTO *> rto_list;
    std::list<FreeChunk> free_list;
    size_t heap_size;
    size_t memory_used;

    uint64_t allocations_count;
    std::ofstream& log_file;
    Globals& globals;
    OperandStack& operand_stack;
    FrameStack& frame_stack;
    TmpRoot& tmp_root;

    uint64_t getTimestamp(void) const;
    size_t computeOffset(const void *ptr, size_t alignment) const;
    void *realAlloc(size_t size, size_t alignment);
    void insertFreeChunk(void *address, size_t size, bool coalesce);
    void freeRTO(RTO *ptr);

public:
    Heap(size_t pheap_size, std::ofstream& plog_file, Globals& pglobals, OperandStack& poperand_stack, FrameStack& pframe_stack, TmpRoot& ptmp_root);
    ~Heap(void);
    void cleanup(void);

    void garbageCollect(void);
    void *alloc(size_t size, size_t alignment);
    void free(void *ptr);

    void registerRTO(RTO& rto);
    void logEvent(char type);
};

class HeapRTOAllocator{
protected:
    Heap& heap;

    template<class T, class... Ts>
    T& universalNew(Ts&&... args);

public:
    HeapRTOAllocator(Heap& pheap);

    RTONull& newNull(void);
    RTOBoolean& newBoolean(bool value);
    RTOInteger& newInteger(fml_int value);
    RTOObject& newObject(void);
    RTOArray& newArray(fml_int size, RTO& filler);
};

#endif
