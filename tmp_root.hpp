#ifndef TMP_ROOT_H
#define TMP_ROOT_H

#include "heap.hpp"

#include <set>

class TmpRoot{
protected:
    std::set<RTO *> temporaries;

public:
    ~TmpRoot(void);
    void cleanup(void);

    void insert(RTO& rto);

    void mark(void);
};

#endif
